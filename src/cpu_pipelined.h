#ifndef MIPS_SIM_CPU_PIPELINED_H
#define MIPS_SIM_CPU_PIPELINED_H

#include "common.h"
#include "umode.h"
#include <cassert>
#include <initializer_list>

class CPU_Pipelined
{
public:
	CPU_Pipelined(UserModeController *controller, bool bAllowMultilock = true) : 
		controller(controller), bAllowMultilock(bAllowMultilock) {}
	void run();

	size_t getCycle() const { return cntCycle; }
	size_t getInstructionCount() const { return cntIns; }

protected:
	UserModeController *controller;
	uint32_t reg[35];
	int reglock[35];

	size_t cntIns, cntCycle;
	bool bAllowMultilock;

protected:
	enum oper
	{
		nop = 0,
		add, sub, mult, div, mod, multu, divu, modu,
		and, or, nor, xor,
		sll, srl, sra, ror,
		seq, slt, sltu,
		mfhi, mflo, mthi, mtlo,
		
		lb, lbu, lh, lhu, lw, sb, sh, sw, 
	};
	struct instruction
	{
		oper alu_op;
		uint32_t alu_in[4], alu_out;
		oper mem_op;
		uint32_t mem_in, mem_out;
		bool syscall;
		MIPSRegister jal;
		MIPSRegister write_to;
		uint32_t instruction::*write_from;
	};

	uint32_t stage1;
	instruction ins[4];
	int ins_stage[5];

protected:
	void fetch();
	bool decode();
	void calculate();
	bool mem_access();
	void write_back();

	bool check_lock(uint8_t reg)
	{
		return reg == 0 || !reglock[reg];
	}
	bool check_lock(std::initializer_list<uint8_t> regs)
	{
		for (uint8_t reg : regs)
			if (reg && reglock[reg])
				return false;
		return true;
	}
	void lock_reg(uint8_t reg)
	{
		if (reg)
		{
			//fprintf(stderr, "Lock %d\n", (int)reg);
			reglock[reg]++;
		}
	}
	void lock_reg(std::initializer_list<uint8_t> regs)
	{
		for (uint8_t reg : regs)
			lock_reg(reg);
	}
	void unlock_reg(uint8_t reg)
	{
		if (reg)
		{
			//fprintf(stderr, "Unlock %d\n", (int)reg);
			assert(reglock[reg]);
			reglock[reg]--;
		}
	}
	void unlock_reg(std::initializer_list<uint8_t> regs)
	{
		for (uint8_t reg : regs)
		{
			unlock_reg(reg);
		}
	}

	static constexpr Opcd decode_opcd(uint32_t ins)
	{
		return Opcd((ins >> 26) & 0x3f);
	}
	static constexpr OpFunc decode_func(uint32_t ins)
	{
		return OpFunc(ins & 0x3f);
	}
	static constexpr uint8_t decode_rs(uint32_t ins)
	{
		return (ins >> 21) & 0x1f;
	}
	static constexpr uint8_t decode_rt(uint32_t ins)
	{
		return (ins >> 16) & 0x1f;
	}
	static constexpr uint8_t decode_rd(uint32_t ins)
	{
		return (ins >> 11) & 0x1f;
	}
	static constexpr uint8_t decode_sa(uint32_t ins)
	{
		return (ins >> 6) & 0x1f;
	}
	static constexpr uint16_t decode_im(uint32_t ins)
	{
		return ins & 0xffff;
	}
	static constexpr uint32_t decode_addr(uint32_t ins)
	{
		return ins & 0x03ffffff;
	}

	static constexpr uint16_t decodeU_im1(uint32_t ins)
	{
		return (ins >> 16) & 0x3ff;
	}
	static constexpr uint16_t decodeU_im2(uint32_t ins)
	{
		return ins & 0x7ff;
	}
};

#endif