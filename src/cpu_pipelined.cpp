#include "cpu_pipelined.h"
#include <cstring>
#include <iostream>

void CPU_Pipelined::run()
{
	controller->getRegisterInitval(reg);
	memset(&reglock, 0, sizeof(reglock));
	memset(&ins, 0, sizeof(ins));
	cntIns = cntCycle = 0;
	stage1 = 0;
	for (int i = 0; i < 4; i++)
		ins_stage[i] = i;
	while (true)
	{
		cntCycle++;
		write_back();
		bool flag = mem_access();
		calculate();
		if (decode() && !flag)
			fetch();
	}
}

void CPU_Pipelined::fetch()
{
	if (reglock[PC])
		stage1 = 0;	//nop
	else
	{
		cntIns++;
		stage1 = controller->getInstruction(reg[PC]);
		//std::cerr << "fetch " << std::hex << reg[PC] << ": " << std::hex << stage1 << " (sp=" << std::hex << reg[sp] << ")" << std::endl;
		//fprintf(stderr, "fetch %x: %08x (sp=%x, fp=%x, v0=%x, t2=%x, t3=%x)\n", reg[PC], stage1, reg[sp], reg[fp], reg[v0],
		//	reg[t2], reg[t3]);
		reg[PC] += 4;
	}
}

bool CPU_Pipelined::decode()
{
	Opcd opcd = decode_opcd(stage1);
	ins_stage[0] = ins_stage[3];
	instruction &now = ins[ins_stage[0]];
	memset(&now, 0, sizeof(now));
	if (opcd == Opcd::SPECIAL)
	{
		OpFunc func = decode_func(stage1);
		uint8_t rs = decode_rs(stage1), rt = decode_rt(stage1), rd = decode_rd(stage1), sa = decode_sa(stage1);
		switch (func)
		{
		case OpFunc::ADD: case OpFunc::ADDU: case OpFunc::SUB: case OpFunc::SUBU:
		case OpFunc::SLLV: case OpFunc::SRLV: case OpFunc::SRAV:
		case OpFunc::AND: case OpFunc::OR: case OpFunc::NOR: case OpFunc::XOR:
		{
			static const std::unordered_map<int, oper> mapOper = {
				{OpFunc::ADD, add}, {OpFunc::ADDU, add}, {OpFunc::SUB, sub}, {OpFunc::SUBU, sub},
				{OpFunc::SLLV, sll}, {OpFunc::SRLV, srl}, {OpFunc::SRAV, sra},
				{OpFunc::AND, and}, {OpFunc::OR, or }, {OpFunc::NOR, nor}, {OpFunc::XOR, xor} };
			if (!check_lock({ rs, rt }))
				return false;
			if (func == OpFunc::SRLV && sa == 0x1)
				now.alu_op = ror;
			else
				now.alu_op = mapOper.find(func)->second;
			now.alu_in[0] = reg[rs];
			now.alu_in[1] = reg[rt];
			now.write_from = &instruction::alu_out;
			now.write_to = MIPSRegister(rd);
			lock_reg(rd);
			break;
		}
		case OpFunc::SLL: case OpFunc::SRA: case OpFunc::SRL:
		{
			static const std::unordered_map<int, oper> mapOper = {
				{OpFunc::SLL, sll}, {OpFunc::SRA, sra}, {OpFunc::SRL, srl} };
			if (!check_lock(rt))
				return false;
			if (func == OpFunc::SRL && rs == 0x1)
				now.alu_op = ror;
			else
				now.alu_op = mapOper.find(func)->second;
			now.alu_in[0] = sa;
			now.alu_in[1] = reg[rt];
			now.write_from = &instruction::alu_out;
			now.write_to = MIPSRegister(rd);
			lock_reg(rd);
			break;
		}
		case OpFunc::MULT: case OpFunc::MULTU:
			if (!check_lock({ rs, rt }))
				return false;
			now.alu_op = func == OpFunc::MULT ? mult : multu;
			now.alu_in[0] = reg[rs];
			now.alu_in[1] = reg[rt];
			if (sa == 0b10)
			{
				now.write_from = &instruction::alu_out;
				now.write_to = MIPSRegister(rd);
				lock_reg(rd);
			}
			break;
		case OpFunc::DIV: case OpFunc::DIVU:
			if (!check_lock({ rs, rt }))
				return false;
			now.alu_op = func == OpFunc::DIV ? div : divu;
			now.alu_in[0] = reg[rs];
			now.alu_in[1] = reg[rt];
			if (sa == 0b10)
			{
				now.write_from = &instruction::alu_out;
				now.write_to = MIPSRegister(rd);
				lock_reg(rd);
			}
			else if (sa == 0b11)
			{
				now.alu_op = func == OpFunc::DIV ? mod : modu;
				now.write_from = &instruction::alu_out;
				now.write_to = MIPSRegister(rd);
				lock_reg(rd);
			}
			break;
		case OpFunc::JR:
			if (!check_lock(rs))
				return false;
			now.alu_out = reg[rs];
			now.write_from = &instruction::alu_out;
			now.write_to = PC;
			lock_reg(PC);
			break;
		case OpFunc::JALR:
			if (!check_lock(rs))
				return false;
			now.alu_out = reg[rs];
			now.write_from = &instruction::alu_out;
			now.write_to = PC;
			now.jal = MIPSRegister(rd);
			lock_reg({ PC, rd });
			break;
		case OpFunc::MFHI: case OpFunc::MFLO:
			now.alu_op = func == OpFunc::MFHI ? mfhi : mflo;
			now.write_from = &instruction::alu_out;
			now.write_to = MIPSRegister(rd);
			lock_reg(rd);
			break;
		case OpFunc::MTHI: case OpFunc::MTLO:
			if (!check_lock(rs))
				return false;
			now.alu_op = func == OpFunc::MTHI ? mthi : mtlo;
			break;
		case OpFunc::SLTU:
			if (!check_lock({ rs, rt }))
				return false;
			now.alu_in[0] = reg[rs];
			now.alu_in[1] = reg[rt];
			if (sa == 0x0)			//sltu
			{
				now.alu_op = sltu;
				now.alu_in[2] = 1;
			}
			else if (sa == 0x1)		//sgeu
			{
				now.alu_op = sltu;
				now.alu_in[3] = 1;
			}
			else if (sa == 0x2)		//seq
			{
				now.alu_op = seq;
				now.alu_in[2] = 1;
			}
			else if (sa == 0x3)		//sne
			{
				now.alu_op = seq;
				now.alu_in[3] = 1;
			}
			else
				throw cpuSignal(cpuSignal::sigInvalidInstruction, reg[PC]);
			now.write_from = &instruction::alu_out;
			now.write_to = MIPSRegister(rd);
			lock_reg(rd);
			break;
		case OpFunc::SLT:
			if (!check_lock({ rs, rt }))
				return false;
			now.alu_in[0] = reg[rs];
			now.alu_in[1] = reg[rt];
			now.alu_op = slt;
			if (sa == 0x0)
				now.alu_in[2] = 1;
			else if (sa == 0x1)		//sge
				now.alu_in[3] = 1;
			else
				throw cpuSignal(cpuSignal::sigInvalidInstruction, reg[PC]);
			now.write_from = &instruction::alu_out;
			now.write_to = MIPSRegister(rd);
			lock_reg(rd);
			break;
		case OpFunc::SYSCALL:
			if (!check_lock({ v0, a0, a1 }))
				return false;
			now.syscall = true;
			lock_reg(PC);
			break;
		case OpFunc::TEQ:
			if (!check_lock({ rs, rt }))
				break;
			if (reg[rs] == reg[rt])
				throw cpuSignal(cpuSignal::sigTrap, reg[PC]);
			break;
		case OpFunc::BREAK:
			throw cpuSignal(cpuSignal::sigTrap, reg[PC]);
			break;
		default:
			throw cpuSignal(cpuSignal::sigInvalidInstruction, reg[PC]);
		}
	}
	else if (opcd == Opcd::SPECIAL2)
	{
		if ((int)decode_func(stage1) == 0b10)
		{
			uint8_t rs = decode_rs(stage1), rt = decode_rt(stage1), rd = decode_rd(stage1);
			if (!check_lock({ rs, rt }))
				return false;
			now.alu_op = mult;
			now.alu_in[0] = reg[rs];
			now.alu_in[1] = reg[rt];
			now.write_from = &instruction::alu_out;
			now.write_to = MIPSRegister(rd);
			lock_reg(rd);
		}
		else
			throw cpuSignal(cpuSignal::sigInvalidInstruction, reg[PC]);
	}
	else if (opcd == Opcd::BEQI_FakeUgly || opcd == Opcd::BNEI_FakeUgly || 
		opcd == Opcd::BGEI_FakeUgly || opcd == Opcd::BLTI_FakeUgly)
	{
		uint32_t im1 = decodeU_im1(stage1), im2 = decodeU_im2(stage1);
		if (im1 & 0x200)
			im1 |= 0xfffffc00;
		if (im2 & 0x400)
			im2 |= 0xfffff800;
		uint8_t rd = decode_rd(stage1);
		if (!check_lock(rd))
			return false;
		now.alu_op = (opcd == Opcd::BLTI_FakeUgly || opcd == Opcd::BGEI_FakeUgly) ? slt : seq;
		now.alu_in[0] = reg[rd];
		now.alu_in[1] = im1;
		if (opcd == Opcd::BLTI_FakeUgly || opcd == Opcd::BEQI_FakeUgly)
		{
			now.alu_in[2] = reg[PC] + int(im2) * 4;
			now.alu_in[3] = reg[PC];
		}
		else
		{
			now.alu_in[3] = reg[PC] + int(im2) * 4;
			now.alu_in[2] = reg[PC];
		}
		now.write_from = &instruction::alu_out;
		now.write_to = PC;
		lock_reg(PC);
	}
	else
	{
		uint8_t rs = decode_rs(stage1), rt = decode_rt(stage1);
		uint32_t im = (int16_t)decode_im(stage1);
		uint32_t addr = decode_addr(stage1);
		switch (opcd)
		{
		case Opcd::DIVI_Fake: case Opcd::DIVIU_Fake: case Opcd::MULI_Fake: case Opcd::REMI_Fake: case Opcd::REMIU_Fake:
		case Opcd::ADDI: case Opcd::ADDIU: case Opcd::ANDI:
		case Opcd::ORI: case Opcd::XORI:
		{
			static const std::unordered_map<int, oper> mapOper = {
				{Opcd::DIVI_Fake, div}, {Opcd::DIVIU_Fake, divu}, {Opcd::MULI_Fake, mult}, {Opcd::REMI_Fake, mod}, {Opcd::REMIU_Fake, modu},
				{Opcd::ADDI, add}, {Opcd::ADDIU, add}, {Opcd::ANDI, and},
				{Opcd::ORI, or }, {Opcd::XORI, xor} };
			if (!check_lock(rs))
				return false;
			now.alu_op = mapOper.find(opcd)->second;
			now.alu_in[0] = reg[rs];
			now.alu_in[1] = im;
			now.write_from = &instruction::alu_out;
			now.write_to = MIPSRegister(rt);
			lock_reg(rt);
			break;
		}
		case Opcd::BEQ: case Opcd::BNE:
			if (!check_lock({ rs, rt }))
				return false;
			now.alu_op = seq;
			now.alu_in[0] = reg[rs];
			now.alu_in[1] = reg[rt];
			if (opcd == Opcd::BEQ)
			{
				now.alu_in[2] = reg[PC] + (int)im * 4;
				now.alu_in[3] = reg[PC];
			}
			else
			{
				now.alu_in[2] = reg[PC];
				now.alu_in[3] = reg[PC] + (int)im * 4;
			}
			now.write_from = &instruction::alu_out;
			now.write_to = PC;
			lock_reg(PC);
			break;
		case Opcd::BGE_Fake: case Opcd::BLT_Fake: case Opcd::BLE_Fake: case Opcd::BGT_Fake:
			if (!check_lock({ rs, rt }))
				return false;
			now.alu_op = slt;
			if (opcd == Opcd::BLT_Fake || opcd == Opcd::BGE_Fake)
			{
				now.alu_in[0] = reg[rs];
				now.alu_in[1] = reg[rt];
			}
			else
			{
				now.alu_in[0] = reg[rt];
				now.alu_in[1] = reg[rs];
			}
			if (opcd == Opcd::BLT_Fake || opcd == Opcd::BGT_Fake)
			{
				now.alu_in[2] = reg[PC] + int(im) * 4;
				now.alu_in[3] = reg[PC];
			}
			else
			{
				now.alu_in[2] = reg[PC];
				now.alu_in[3] = reg[PC] + int(im) * 4;
			}
			now.write_from = &instruction::alu_out;
			now.write_to = PC;
			lock_reg(PC);
			break;
		case Opcd::J: case Opcd::JAL:
			now.alu_out = (reg[PC] & 0xfc000000) | (addr << 2);
			now.write_from = &instruction::alu_out;
			now.write_to = PC;
			if (opcd == Opcd::JAL)
			{
				now.jal = ra;
				lock_reg({ PC, ra });
			}
			else
				lock_reg(PC);
			break;
		case Opcd::BC: case Opcd::BALC:
		{
			int offset = int(addr & 0x02000000 ? (addr | 0xfc000000) : addr) * 4;
			now.alu_out = reg[PC] + offset;
			now.write_from = &instruction::alu_out;
			now.write_to = PC;
			if (opcd == Opcd::BALC)
			{
				now.jal = ra;
				lock_reg({ PC, ra });
			}
			else
				lock_reg(PC);
			break;
		}
		case Opcd::BGEZ: 
			if (!check_lock(rs))
				return false;
			now.alu_op = slt;
			now.alu_in[0] = reg[rs];
			now.alu_in[1] = 0;
			if (rt == 0x0)		//bltz
			{
				now.alu_in[2] = reg[PC] + int(im) * 4;
				now.alu_in[3] = reg[PC];
			}
			else				//bgez
			{
				now.alu_in[2] = reg[PC];
				now.alu_in[3] = reg[PC] + int(im) * 4;
			}
			now.write_from = &instruction::alu_out;
			now.write_to = PC;
			lock_reg(PC);
			break;
		case Opcd::BGTZ: case Opcd::BLEZ:
			if (!check_lock(rs))
				return false;
			now.alu_op = slt;
			now.alu_in[0] = 0;
			now.alu_in[1] = reg[rs];
			if (opcd == Opcd::BGTZ)
			{
				now.alu_in[2] = reg[PC] + int(im) * 4;
				now.alu_in[3] = reg[PC];
			}
			else
			{
				now.alu_in[2] = reg[PC];
				now.alu_in[3] = reg[PC] + int(im) * 4;
			}
			now.write_from = &instruction::alu_out;
			now.write_to = PC;
			if (rs == rt && rs != 0)
			{
				now.jal = ra;
				lock_reg({ PC, ra });
			}
			else if(rt == 0)
				lock_reg(PC);
			break;
		case Opcd::SEQI_Fake: case Opcd::SNEI_Fake:
		case Opcd::SLTI: case Opcd::SGEI_Fake:
			if (!check_lock(rs))
				return false;
			now.alu_op = (opcd == Opcd::SEQI_Fake || opcd == Opcd::SNEI_Fake) ? seq : slt;
			now.alu_in[0] = reg[rs];
			now.alu_in[1] = im;
			if (opcd == Opcd::SEQI_Fake || opcd == Opcd::SLTI)
				now.alu_in[2] = 1;
			else
				now.alu_in[3] = 1;
			now.write_from = &instruction::alu_out;
			now.write_to = MIPSRegister(rt);
			lock_reg(rt);
			break;
		case Opcd::SLTIU: case Opcd::SGEIU_Fake:
			if (!check_lock(rs))
				return false;
			now.alu_op = sltu;
			now.alu_in[0] = reg[rs];
			now.alu_in[1] = im;
			if (opcd == Opcd::SLTIU)
				now.alu_in[2] = 1;
			else
				now.alu_in[3] = 1;
			now.write_from = &instruction::alu_out;
			now.write_to = MIPSRegister(rt);
			lock_reg(rt);
			break;
		case Opcd::LB: case Opcd::LBU: case Opcd::LH: case Opcd::LHU: case Opcd::LW:
		{
			static const std::unordered_map<int, oper> mapOper = {
				{Opcd::LB, lb}, {Opcd::LBU, lbu}, {Opcd::LH, lh}, {Opcd::LHU, lhu}, {Opcd::LW, lw} };
			if (!check_lock(rs))
				return false;
			now.alu_out = reg[rs] + int(im);	//I'm lazy.
			now.mem_op = mapOper.find(opcd)->second;
			now.write_from = &instruction::mem_out;
			now.write_to = MIPSRegister(rt);
			lock_reg(rt);
			break;
		}
		case Opcd::SB: case Opcd::SH: case Opcd::SW:
		{
			static const std::unordered_map<int, oper> mapOper = {
				{Opcd::SB, sb}, {Opcd::SH, sh}, {Opcd::SW, sw} };
			if (!check_lock({ rs, rt }))
				return false;
			now.alu_out = reg[rs] + int(im);
			now.mem_in = reg[rt];
			now.mem_op = mapOper.find(opcd)->second;
			break;
		}
		case Opcd::LUI:
			now.alu_out = im << 16;	//I'm so lazy. :)
			now.write_from = &instruction::alu_out;
			now.write_to = MIPSRegister(rt);
			lock_reg(rt);
			break;
		default:
			throw cpuSignal(cpuSignal::sigInvalidInstruction, reg[PC]);
		}
	}
	stage1 = 0;
	return true;
}

void CPU_Pipelined::calculate()
{
	ins_stage[1] = ins_stage[0];
	instruction &now = ins[ins_stage[1]];
	uint64_t tmp = 0;
	switch (now.alu_op)
	{
	case add:
		now.alu_out = now.alu_in[0] + now.alu_in[1];
		break;
	case sub:
		now.alu_out = now.alu_in[0] - now.alu_in[1];
		break;
	case mult:
		tmp = (int64_t)now.alu_in[0] * (int64_t)now.alu_in[1];
		reg[LO] = tmp & 0xFFFFFFFFLL;
		reg[HI] = (tmp & 0xFFFFFFFF00000000LL) >> 32;
		now.alu_out = reg[LO];
		break;
	case div:
		reg[LO] = (int32_t)now.alu_in[0] / (int32_t)now.alu_in[1];
		reg[HI] = (int32_t)now.alu_in[0] % (int32_t)now.alu_in[1];
		now.alu_out = reg[LO];
		break;
	case mod:
		reg[LO] = (int32_t)now.alu_in[0] / (int32_t)now.alu_in[1];
		reg[HI] = (int32_t)now.alu_in[0] % (int32_t)now.alu_in[1];
		now.alu_out = reg[HI];
		break;
	case multu:
		tmp = (uint64_t)now.alu_in[0] * now.alu_in[1];
		reg[LO] = tmp & 0xFFFFFFFFLL;
		reg[HI] = (tmp & 0xFFFFFFFF00000000LL) >> 32;
		now.alu_out = reg[LO];
		break;
	case divu:
		reg[LO] = now.alu_in[0] / now.alu_in[1];
		reg[HI] = now.alu_in[0] % now.alu_in[1];
		now.alu_out = reg[LO];
		break;
	case modu:
		reg[LO] = now.alu_in[0] / now.alu_in[1];
		reg[HI] = now.alu_in[0] % now.alu_in[1];
		now.alu_out = reg[HI];
		break;
	case and:
		now.alu_out = now.alu_in[0] & now.alu_in[1];
		break;
	case or:
		now.alu_out = now.alu_in[0] | now.alu_in[1];
		break;
	case nor:
		now.alu_out = ~(now.alu_in[0] | now.alu_in[1]);
		break;
	case xor:
		now.alu_out = now.alu_in[0] ^ now.alu_in[1];
		break;
	case sll:
		now.alu_out = now.alu_in[1] << now.alu_in[0];
		break;
	case srl:
		now.alu_out = now.alu_in[1] >> now.alu_in[0];
		break;
	case sra:
		now.alu_out = (int32_t)now.alu_in[1] >> now.alu_in[0];
		break;
	case ror:
		now.alu_out = (now.alu_in[1] << (32 - now.alu_in[0])) | (now.alu_in[1] >> now.alu_in[0]);
		break;
	case seq:
		now.alu_out = (now.alu_in[0] == now.alu_in[1]) ? now.alu_in[2] : now.alu_in[3];
		break;
	case slt:
		now.alu_out = ((int32_t)now.alu_in[0] < (int32_t)now.alu_in[1]) ? now.alu_in[2] : now.alu_in[3];
		break;
	case sltu:
		now.alu_out = (now.alu_in[0] < now.alu_in[1]) ? now.alu_in[2] : now.alu_in[3];
		break;
	case mfhi:
		now.alu_out = reg[HI];
		break;
	case mflo:
		now.alu_out = reg[LO];
		break;
	case mthi:
		reg[HI] = now.alu_in[0];
		break;
	case mtlo:
		reg[LO] = now.alu_in[0];
		break;
	default:
		assert(now.alu_op == nop);
	}
}

bool CPU_Pipelined::mem_access()
{
	ins_stage[2] = ins_stage[1];
	instruction &now = ins[ins_stage[2]];
	switch (now.mem_op)
	{
	case lb:
		now.mem_out = (int8_t)controller->getByte(now.alu_out);
		break;
	case lbu:
		now.mem_out = controller->getByte(now.alu_out);
		break;
	case lh:
		now.mem_out = (int16_t)controller->getHalfword(now.alu_out);
		break;
	case lhu:
		now.mem_out = controller->getHalfword(now.alu_out);
		break;
	case lw:
		now.mem_out = controller->getWord(now.alu_out);
		break;
	case sb:
		controller->saveByte(now.alu_out, now.mem_in);
		break;
	case sh:
		controller->saveHalfword(now.alu_out, now.mem_in);
		break;
	case sw:
		controller->saveWord(now.alu_out, now.mem_in);
		break;
	default:
		assert(now.mem_op == nop);
		return false;
	}
	return true;
}

void CPU_Pipelined::write_back()
{
	ins_stage[3] = ins_stage[2];
	instruction &now = ins[ins_stage[3]];
	if (now.syscall)
	{
		controller->syscall(reg);
		unlock_reg(PC);
	}
	if (now.jal)
	{
		reg[now.jal] = reg[PC];
		unlock_reg(now.jal);
	}
	if (now.write_to != zero)
	{
		//if(now.write_to == ra)
			//fprintf(stderr, "Write [%d] = %x -> %x\n", (int)now.write_to, reg[now.write_to], now.*(now.write_from));
		reg[now.write_to] = now.*(now.write_from);
	}
	
	unlock_reg(now.write_to);
}
