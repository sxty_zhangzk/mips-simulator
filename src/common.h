#ifndef MIPS_SIM_COMMON_H
#define MIPS_SIM_COMMON_H

#include "define.h"

constexpr uint32_t ltob(uint32_t x)
{
	return ((x & 0xff000000) >> 24) | ((x & 0x00ff0000) >> 8) | ((x & 0x0000ff00) << 8) | ((x & 0x000000ff) << 24);
}
constexpr uint16_t ltob(uint16_t x)
{
	return ((x & 0xff00) >> 8) | ((x & 0x00ff) << 8);
}
constexpr uint32_t alignAddr(uint32_t addr, int align)
{
	return (addr + (1 << align) - 1) & ~((1 << align) - 1);
}
constexpr uint32_t alignBack(uint32_t addr, int align)
{
	return addr & ~((1 << align) - 1);
}

#endif