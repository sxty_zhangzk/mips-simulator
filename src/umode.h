#ifndef MIPS_SIM_UMODE_H
#define MIPS_SIM_UMODE_H

#include "common.h"
#include <set>
#include <map>
#include <unordered_map>

class UserModeController
{
public:
	bool loadElfData(const byte_t *buffer, size_t size);
	void setGPAddr(uint32_t gp) { gpAddr = gp; }

public:
	void syscall(uint32_t reg[]);
	void getRegisterInitval(uint32_t reg[]);
	uint32_t getInstruction(uint32_t addr);
	uint32_t getWord(uint32_t addr);
	uint16_t getHalfword(uint32_t addr);
	byte_t getByte(uint32_t addr);
	void saveWord(uint32_t addr, uint32_t data);
	void saveHalfword(uint32_t addr, uint16_t data);
	void saveByte(uint32_t addr, byte_t data);

protected:
	static const uint32_t pageAlign = 12;
	static const uint32_t pageSize = 1 << pageAlign;
	static const uint32_t addrMask = pageSize - 1;
	static const uint32_t pageMask = ~addrMask;
	static const uint32_t stackExtendSize = 16 * pageSize;

	struct page
	{
		byte_t *buffer;
		uint32_t flag;
	};

	uint32_t addrStack, addrHeap;
	uint32_t gpAddr;
	uint32_t addrEntry;
	std::unordered_map<uint32_t, page> pageMapping;

	page newPage(uint32_t addr, uint32_t flag);
	bool extendStack(uint32_t addr);
	page getPage(uint32_t addr, uint32_t flag);

private:
	static const uint32_t baseAddrStack = 0x80000000;
};

#endif