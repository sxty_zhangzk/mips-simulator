#include "umode.h"
#include <cstdio>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <iostream>
#include <string>

bool UserModeController::loadElfData(const byte_t *buffer, size_t size)
{
	if (buffer[0] != 0x7F || buffer[1] != 'E' || buffer[2] != 'L' || buffer[3] != 'F')
		return false;
	Elf32_Ehdr ehdr;
	memcpy(&ehdr, buffer, sizeof(Elf32_Ehdr));
	uint32_t phoffset = ltob(ehdr.e_phoff);
	uint32_t topAddr = 0;
	for (int i = 0; i < ltob(ehdr.e_phnum); i++)
	{
		Elf32_Phdr phdr;
		memcpy(&phdr, buffer + phoffset + i * ltob(ehdr.e_phentsize), ltob(ehdr.e_phentsize));
		if (ltob(phdr.p_type) != PT_LOAD)
			continue;
		if (ltob(phdr.p_align) < (1 << pageAlign))
			return false;

		uint32_t offset = alignBack(ltob(phdr.p_offset), pageAlign);
		uint32_t addr = alignBack(ltob(phdr.p_paddr), pageAlign);
		uint32_t addrEnd = ltob(phdr.p_paddr) + ltob(phdr.p_memsz);
		
		for (; addr < addrEnd; addr += (1 << pageAlign), offset += (1 << pageAlign))
		{
			page temp = newPage(addr, ltob(phdr.p_flags));
			size_t szCopy = std::min(size - offset, (size_t)1 << pageAlign);
			memcpy(temp.buffer, buffer + offset, szCopy);
		}

		topAddr = std::max(topAddr, addr);
	}
	addrEntry = ltob(ehdr.e_entry);
	addrHeap = topAddr;
	addrStack = baseAddrStack;

	newPage(addrHeap, PF_R | PF_W);

	return true;
}

UserModeController::page UserModeController::newPage(uint32_t addr, uint32_t flag)
{
	assert((addr & addrMask) == 0);
	page temp;
	temp.buffer = new byte_t[1 << pageAlign];
	temp.flag = flag;
	memset(temp.buffer, 0, 1 << pageAlign);
	pageMapping.insert(std::make_pair(addr, temp));
	return temp;
}

void UserModeController::syscall(uint32_t reg[])
{
	switch (reg[v0])
	{
	case 1:
		printf("%d", (int)reg[a0]);
		break;
	case 4:
	{
		uint32_t addr = reg[a0];
		char c;
		while ((c = getByte(addr++)) != '\0')
			putchar(c);
		break;
	}
	case 5:
		scanf("%d", (int *)&reg[v0]);
		break;
	case 8:
	{
		std::string buffer;
		std::cin >> buffer;

		int cnt = 0;
		for (; cnt < reg[a1] - 1 && cnt < buffer.size(); cnt++)
		{
			saveByte(reg[a0] + cnt, buffer[cnt]);
		}
		saveByte(reg[a0] + cnt, 0);
		break;
	}
	case 9:
	{
		//fprintf(stderr, "SBRK %u: %x\n", reg[a0], addrHeap);
		reg[v0] = addrHeap;
		int remain = reg[a0];
		if (remain == 0)
			break;
		while (((addrHeap + remain) & pageMask) != (addrHeap & pageMask))
		{
			newPage(alignAddr(addrHeap, pageAlign), PF_R | PF_W);
			addrHeap += pageSize;
			remain -= pageSize;
		}
		addrHeap += remain;
		break;
	}
	case 10:
		throw cpuSignal(cpuSignal::sigExit);
		break;
	case 17:
		throw cpuSignal(cpuSignal::sigExit, reg[a0]);
		break;
	default:
		throw cpuSignal(cpuSignal::sigInvalidSyscall, reg[v0]);
	}
}

void UserModeController::getRegisterInitval(uint32_t reg[])
{
	for (int i = zero; i <= LO; i++)
		reg[i] = 0;
	reg[sp] = addrStack;
	reg[gp] = gpAddr;
	reg[PC] = addrEntry;
}

bool UserModeController::extendStack(uint32_t addr)
{
	if (addr < addrStack && addrStack - addr <= stackExtendSize)
	{
		while (addr < addrStack)
		{
			newPage(addrStack - pageSize, PF_R | PF_W);
			addrStack -= pageSize;
		}
		return true;
	}
	return false;
}

UserModeController::page UserModeController::getPage(uint32_t addr, uint32_t flag)
{
	auto iter = pageMapping.find(addr & pageMask);
	if (iter == pageMapping.end())
	{
		if (!extendStack(addr))
			throw cpuSignal(cpuSignal::sigSegmentFault);
		else
		{
			iter = pageMapping.find(addr & pageMask);
			assert(iter != pageMapping.end());
		}
	}
	if (!(iter->second.flag & flag))
		throw cpuSignal(cpuSignal::sigSegmentFault);
	return iter->second;
}

uint32_t UserModeController::getInstruction(uint32_t addr)
{
	auto iter = pageMapping.find(addr & pageMask);
	if (iter == pageMapping.end())
		throw cpuSignal(cpuSignal::sigSegmentFault);
	if(!(iter->second.flag & PF_X))
		throw cpuSignal(cpuSignal::sigSegmentFault);
	if (addr & 0x3)
		throw cpuSignal(cpuSignal::sigAlignFault);
	return ltob(*(uint32_t *)(iter->second.buffer + (addr & addrMask)));
}

uint32_t UserModeController::getWord(uint32_t addr)
{
	page tmp = getPage(addr, PF_R);
	//if (addr & 0x3)
	//	throw cpuSignal(cpuSignal::sigAlignFault);
	return ltob(*(uint32_t *)(tmp.buffer + (addr & addrMask)));
}

uint16_t UserModeController::getHalfword(uint32_t addr)
{
	page tmp = getPage(addr, PF_R);
	if (addr & 0x1)
		throw cpuSignal(cpuSignal::sigAlignFault);
	return ltob(*(uint16_t *)(tmp.buffer + (addr & addrMask)));
}

byte_t UserModeController::getByte(uint32_t addr)
{
	page tmp = getPage(addr, PF_R);
	return *(tmp.buffer + (addr & addrMask));
}

void UserModeController::saveWord(uint32_t addr, uint32_t data)
{
	page tmp = getPage(addr, PF_W);
	//if(addr & 0x3)
	//	throw cpuSignal(cpuSignal::sigAlignFault);
	*(uint32_t *)(tmp.buffer + (addr & addrMask)) = ltob(data);
}

void UserModeController::saveHalfword(uint32_t addr, uint16_t data)
{
	page tmp = getPage(addr, PF_W);
	if(addr & 0x1)
		throw cpuSignal(cpuSignal::sigAlignFault);
	*(uint16_t *)(tmp.buffer + (addr & addrMask)) = ltob(data);
}

void UserModeController::saveByte(uint32_t addr, byte_t data)
{
	page tmp = getPage(addr, PF_W);
	*(tmp.buffer + (addr & addrMask)) = data;
}
