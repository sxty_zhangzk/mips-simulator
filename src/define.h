#ifndef MIPS_SIM_DEFINE_H
#define MIPS_SIM_DEFINE_H

#include <cstdint>
#include <cstddef>

enum MIPSRegister : uint8_t
{
	zero = 0,
	at = 1,
	v0 = 2, v1 = 3,
	a0 = 4, a1 = 5, a2 = 6, a3 = 7,
	t0 = 8, t1 = 9, t2 = 10, t3 = 11, t4 = 12, t5 = 13, t6 = 14, t7 = 15,
	s0 = 16, s1 = 17, s2 = 18, s3 = 19, s4 = 20, s5 = 21, s6 = 22, s7 = 23,
	t8 = 24, t9 = 25,
	k0 = 26, k1 = 27,
	gp = 28, sp = 29, fp = 30, ra = 31,

	PC = 32, HI = 33, LO = 34
};

typedef uint8_t byte_t;

const size_t EI_NIDENT = 16;
const uint32_t PT_NULL = 0x0;
const uint32_t PT_LOAD = 0x1;
const uint32_t PT_INTERP = 0x3;
const uint32_t PF_X = 1 << 0;
const uint32_t PF_W = 1 << 1;
const uint32_t PF_R = 1 << 2;

typedef uint32_t Elf32_Addr;
typedef uint32_t Elf32_Off;

struct Elf32_Ehdr
{
	unsigned char e_ident[EI_NIDENT];
	uint16_t e_type;
	uint16_t e_machine;
	uint32_t e_version;
	Elf32_Addr e_entry;
	Elf32_Off e_phoff;
	Elf32_Off e_shoff;
	uint32_t e_flags;
	uint16_t e_ehsize;
	uint16_t e_phentsize;
	uint16_t e_phnum;
	uint16_t e_shentsize;
	uint16_t e_shnum;
	uint16_t e_shstrndx;
};

struct Elf32_Phdr
{
	uint32_t p_type;
	Elf32_Off p_offset;
	Elf32_Addr p_vaddr;
	Elf32_Addr p_paddr;
	uint32_t p_filesz;
	uint32_t p_memsz;
	uint32_t p_flags;
	uint32_t p_align;
};

enum Opcd
{
	SPECIAL = 0,
	SPECIAL2 = 0x1B,
	BLTZ = 0x01,
	BGEZ = 0x01,
	BEQ = 0x04,
	BNE = 0x05,
	BLEZ = 0x06, POP06 = 0x06,
	BGTZ = 0x07, POP07 = 0x07,
	ADDI = 0x08,
	ADDIU = 0x09,
	SLTI = 0x0A,
	SLTIU = 0x0B,
	ANDI = 0x0C,
	ORI = 0x0D,
	XORI = 0x0E,
	LUI = 0x0F,
	LB = 0x20,
	LH = 0x21,
	LW = 0x23,
	LBU = 0x24,
	LHU = 0x25,
	SB = 0x28,
	SH = 0x29,
	SW = 0x2B,
	LWC1 = 0x31,
	SWC1 = 0x39,
	J = 0x02,
	JAL = 0x03,
	BC = 0x32,
	BALC = 0x3A,

	DIVI_Fake = 0x3F,
	DIVIU_Fake = 0x33,
	MULI_Fake = 0x34,
	SEQI_Fake = 0x35,
	SNEI_Fake = 0x36,
	SGEI_Fake = 0x37,
	SGEIU_Fake = 0x38,
	BLT_Fake = 0x3B,
	BLE_Fake = 0x3C,
	BGT_Fake = 0x3D,
	BGE_Fake = 0x3E,
	//WTF? TWO Immediate Number in ONE instruction?!
	BEQI_FakeUgly = 0x10,
	BGEI_FakeUgly = 0x11,
	//Skip BGEUI_FakeUgly for it's too ugly.
	BLTI_FakeUgly = 0x12,
	BNEI_FakeUgly = 0x13,
	REMI_Fake = 0x14,
	REMIU_Fake = 0x15
};
enum OpFunc
{
	SLL = 0x00,
	SRL = 0x02,
	SRA = 0x03,
	SLLV = 0x04,
	SRLV = 0x06,
	SRAV = 0x07,
	JR = 0x08,
	JALR = 0x09,
	SYSCALL = 0x0C,
	BREAK = 0x0D,
	MFHI = 0x10,
	MTHI = 0x11,
	MFLO = 0x12,
	MTLO = 0x13,
	MULT = 0x18, SOP30 = 0x18,
	MULTU = 0x19, SOP31 = 0x19,
	DIV = 0x1A, SOP32 = 0x1A,
	DIVU = 0x1B, SOP33 = 0x1B,
	ADD = 0x20,
	ADDU = 0x21,
	SUB = 0x22,
	SUBU = 0x23,
	AND = 0x24,
	OR = 0x25,
	XOR = 0x26,
	NOR = 0x27,
	SLT = 0x2A,		//Fake: SGE 0x1
	SLTU = 0x2B,	//Fake: SGEU 0x1 SEQ 0x2, SNE 0x3
	TEQ = 0x34
};

enum MIPSArch
{
	MIPS32r2, MIPS32r6,
	compatibility,	//MIPS32r6 base, compatible with MIPS32r2 (I hope so)
	fake			//Okay, I know it's not enough
};

struct cpuSignal
{
	enum signal
	{ 
		sigExit, sigOverflow, sigTrap, sigSegmentFault, sigInvalidInstruction, sigInvalidSyscall,
		sigAlignFault
	} sig;
	int value;
	cpuSignal(signal sig, int value = 0) : sig(sig), value(value) {}
};

#endif
