#include "assembler.h"
#include "umode.h"
#include "cpu_pipelined.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		fprintf(stderr, "Not enough arguments\n");
		return -1;
	}
	std::ifstream fin(argv[1]);
	if (!fin)
	{
		fprintf(stderr, "Cannot open file: '%s'\n", argv[1]);
		return -1;
	}

	Assembler asmer(12, fake, true);
	asmer.loadAsm(fin);
	for (auto i : asmer.getError())
		fprintf(stderr, "%s:%d error:  %s\n", argv[1], i.line, i.description.c_str());
	for (auto i : asmer.getWarning())
		fprintf(stderr, "%s:%d warning:  %s\n", argv[1], i.line, i.description.c_str());

	if (!asmer.getError().empty())
	{
		fprintf(stderr, "Assembly terminated.\n");
		return -1;
	}
	byte_t *buffer = new byte_t[asmer.getImageSize()];
	if (!asmer.generateImage(buffer))
	{
		fprintf(stderr, "Cannot generate ELF image.\n");
		return -1;
	}
	UserModeController controller;
	if (!controller.loadElfData(buffer, asmer.getImageSize()))
	{
		fprintf(stderr, "Failed to load ELF data\n");
		return -1;
	}
	controller.setGPAddr(asmer.getGPAddr());
	CPU_Pipelined mipscpu(&controller);
	try
	{
		mipscpu.run();
	}
	catch (cpuSignal &sig)
	{
		if (sig.sig == cpuSignal::sigExit)
		{
			fprintf(stderr, "Program exited normally. Exitcode=%d\n", sig.value);
		}
		else
		{
			fprintf(stderr, "Program terminated. Error code=%d, value=%d\n", sig.sig, sig.value);
		}
		fprintf(stderr, "Instruction: %d, Cycle: %d\n", (int)mipscpu.getInstructionCount(), (int)mipscpu.getCycle());
		return sig.value;
	}
	return 0;
}