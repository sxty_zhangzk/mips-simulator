#ifndef MIPS_SIM_ASSEMBLER_H
#define MIPS_SIM_ASSEMBLER_H

#include "common.h"
#include <string>
#include <vector>
#include <set>
#include <unordered_map>

class Assembler
{
public:
	struct Issue
	{
		int line;
		std::string description;
	};

public:
	Assembler(uint32_t pageAlign = 12, MIPSArch arch = compatibility, bool useGP = false);
	bool loadAsm(std::istream &in);
	const std::vector<Issue> & getError() const { return issError; }
	const std::vector<Issue> & getWarning() const { return issWarn; }
	void reset();
	uint32_t getImageSize() const { return sizeImage; }
	bool generateImage(byte_t *buffer);
	uint32_t getGPAddr() const { return addrGP; }
	void setup(uint32_t newPageAlign, MIPSArch newArch, bool useGP)
	{
		pageAlign = newPageAlign;
		arch = newArch;
		bUseGP = useGP;
		reset();
	}

protected:
	enum TokenType
	{
		INVALID_TokenType = 0,
		asmd, directive, label, reg, number, string, labelname, oper, bracket_in, bracket_out, sect,
		unknown
	};
	struct Token
	{
		TokenType type;
		int value;
		std::string name;

		Token() : type(INVALID_TokenType), value(0) {}
		Token(TokenType type, int value = 0, const std::string &name = "") : type(type), value(value), name(name) {}
	};

	std::vector<Issue> issError, issWarn;
	std::vector<std::vector<Token>> vLines;
	std::vector<uint32_t> vLineAddr, vLineOffset;
	std::vector<int> vLineInsCnt;

	std::unordered_map<std::string, int> mapLabelLine;
	std::unordered_map<int, std::set<int>> mapLocalLLine;		//Label ID, Line ID
	uint32_t sizeImage;

	Elf32_Ehdr elfHeader;
	std::vector<Elf32_Phdr> vElfPhdr;
	uint32_t pageAlign;

	MIPSArch arch;
	bool bUseGP;
	uint32_t addrGP;
	bool bLoaded;

protected:
	std::vector<Token> parseLine(const std::string &line, int lineID);
	void locateLabel();
	int encodeInstruction(const std::vector<Token> &line, int lineID, byte_t *buffer);

private:
	enum ASMerDirective : int;
	enum Directive : int;
	enum Oper : int;
	enum Section : int;
	static const std::unordered_map<std::string, int> mapASMerD;
	static const std::unordered_map<std::string, int> mapD;
	static const std::unordered_map<std::string, int> mapR;
	static const std::unordered_map<std::string, int> mapO;
	static const std::unordered_map<std::string, int> mapS;
	static const int MaxError = 20;
	static const int RegCount = 32;
	static const uint32_t baseAddr = 0x400000;
	static const Elf32_Ehdr elfTemplateHeader;
	
	static bool isSpace(char c)
	{
		return c == '\t' || c == ' ';
	}
	static bool isHex(char c)
	{
		return c >= '0' && c <= '9' || c >= 'a' && c <= 'f' || c >= 'A' && c <= 'F';
	}
	static bool isBreak(char c)
	{
		return isSpace(c) || c == ',' || c == '(' || c == ')';
	}
	static int hex2int(char c)
	{
		if (c >= '0' && c <= '9')
			return c - '0';
		if (c >= 'a' && c <= 'f')
			return c - 'a';
		if (c >= 'A' && c <= 'F')
			return c - 'A';
		return 0;
	}
	static bool checkNum(const std::string &str)
	{
		if (str.empty())
			return false;
		size_t i = 0;
		if (str[0] == '-')
			i = 1;
		for (; i < str.size(); i++)
			if (str[i] < '0' || str[i] > '9')
				return false;
		return true;
	}
	static int checkLabel(const std::string &label);
	static bool checkASMD(const std::vector<Token> &line);
	bool newLabel(const Token &label, int lineID);
	int findLabel(const Token &label, int lineID);

	static constexpr uint32_t encodeRType(OpFunc func, uint8_t rs, uint8_t rt, uint8_t rd, uint8_t sa = 0, Opcd opcd = Opcd::SPECIAL)
	{
		return ltob(uint32_t(((uint8_t)opcd << 26) | (rs << 21) | (rt << 16) | (rd << 11) | (sa << 6) | (uint8_t)func));
	}
	static constexpr uint32_t encodeIType(Opcd opcd, uint8_t rs, uint8_t rt, uint16_t im)
	{
		return ltob(uint32_t(((uint8_t)opcd << 26) | (rs << 21) | (rt << 16) | im));
	}
	static constexpr uint32_t encodeJType(Opcd opcd, uint32_t target)
	{
		return ltob(((uint8_t)opcd << 26) | target);
	}

	//Ugly Type
	static constexpr uint32_t encodeUType(Opcd opcd, uint16_t im1, uint8_t rd, uint16_t im2)	//6, 10, 5, 11
	{
		return ltob(uint32_t(((uint8_t)opcd << 26) | (im1 << 16) | (rd << 11) | im2));
	}
};

#endif
