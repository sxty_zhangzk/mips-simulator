#include "assembler.h"
#include <cassert>
#include <cstring>
#include <sstream>
#include <initializer_list>

enum Assembler::ASMerDirective : int
{
	INVALID_ASMDirective = 0,
	align, ascii, asciiz, byte, section, space, word
};

enum Assembler::Directive : int
{
	INVALID_Directive = 0,
	abs, add, addi, addu, addiu, and, andi, div, divu, mul, mulo, mulou,
	mult, multu,
	neg, negu, nor, not, or , ori, rem, remu, rol, ror, sll, sllv, sra, srav,
	srl, srlv, sub, subu, xor, xori,
	li, lui,
	seq, sge, sgeu, sgt, sgtu, sle, sleu, slt, slti, sltu, sltiu, sne,
	b, beq, beqz, bge, bgeu, bgez, bgt, bgtu, bgtz, ble, bleu, blez,
	bgezal, bltzal, blt, bltu, bltz, bne, bnez,
	j, jal, jalr, jr,
	la, lb, lbu, ld, lh, lhu, lw,
	sb, sd, sh, sw,
	move,
	mflo, mfhi,
	nop, syscall
};

enum Assembler::Oper : int
{
	INVALID_Oper = 0,
	lo, hi
};

enum Assembler::Section : int
{
	INVALID_Section = 0,
	data, text, bss, rdata
};

const std::unordered_map<std::string, int> Assembler::mapASMerD({
	{ "align", align },{ "ascii", ascii },{ "asciiz", asciiz },{ "asciz", asciiz },{ "byte", byte },
	{ "section", section },{ "space", space },{ "word", word } });

const std::unordered_map<std::string, int> Assembler::mapD({
	{"abs", abs}, {"add", add}, {"addi", addi}, {"addu", addu}, { "addiu", addiu }, {"and", and}, {"andi", andi}, {"div", div}, {"divu", divu}, {"mul", mul}, {"mulo", mulo}, {"mulou", mulou},
	{"mult", mult}, {"multu", multu},
	{"neg", neg}, {"negu", negu}, {"nor", nor}, {"not", not}, {"or", or }, {"ori", ori}, {"rem", rem}, {"remu", remu}, {"rol", rol}, {"ror", ror}, {"sll", sll}, {"sllv", sllv}, {"sra", sra}, {"srav", srav},
	{"srl", srl}, {"srlv", srlv}, {"sub", sub}, {"subu", subu}, {"xor", xor}, {"xori", xori},
	{"li", li}, {"lui", lui},
	{"seq", seq}, {"sge", sge}, {"sgeu", sgeu}, {"sgt", sgt}, {"sgtu", sgtu}, {"sle", sle}, {"sleu", sleu}, {"slt", slt}, {"slti", slti}, {"sltu", sltu}, {"sltiu", sltiu}, {"sne", sne},
	{"b", b}, {"beq", beq}, {"beqz", beqz}, {"bge", bge}, {"bgeu", bgeu}, {"bgez", bgez}, {"bgt", bgt}, {"bgtu", bgtu}, {"bgtz", bgtz}, {"ble", ble}, {"blez", blez},
	{"bgezal", bgezal}, {"bltzal", bltzal}, {"blt", blt}, {"bltu", bltu}, {"bltz", bltz}, {"bne", bne}, {"bnez", bnez},
	{"j", j}, {"jal", jal}, {"jalr", jalr}, {"jr", jr},
	{"la", la}, {"lb", lb}, {"lbu", lbu}, {"ld", ld}, {"lh", lh}, {"lhu", lhu}, {"lw", lw},
	{"sb", sb}, {"sd", sd}, {"sh", sh}, {"sw", sw},
	{"move", move},
	{"mflo", mflo}, {"mfhi", mfhi},
	{"nop", nop}, {"syscall", syscall} });

const std::unordered_map<std::string, int> Assembler::mapR({
	{"zero", zero},
	{"at", at},
	{"v0", v0}, {"v1", v1},
	{"a0", a0}, {"a1", a1}, {"a2", a2}, {"a3", a3},
	{"t0", t0}, {"t1", t1}, {"t2", t2}, {"t3", t3}, {"t4", t4}, {"t5", t5}, {"t6", t6}, {"t7", t7},
	{"s0", s0}, {"s1", s1}, {"s2", s2}, {"s3", s3}, {"s4", s4}, {"s5", s5}, {"s6", s6}, {"s7", s7},
	{"t8", t8}, {"t9", t9},
	{"k0", k0}, {"k1", k1},
	{"gp", gp}, {"sp", sp}, {"fp", fp}, {"ra", ra} });

const std::unordered_map<std::string, int> Assembler::mapO = {
	{"lo", lo}, {"hi", hi} };

const std::unordered_map<std::string, int> Assembler::mapS = {
	{"data", data}, {"text", text}, {"bss", bss}, {"rdata", rdata}, {"rodata", rdata} };

const Elf32_Ehdr Assembler::elfTemplateHeader = {
	{0x7f, 'E', 'L','F', 1, 2, 1, 0},
	ltob((uint16_t)2U),
	ltob((uint16_t)0x08U),	//MIPS
	ltob(1U),
	0,
	0,
	0,
	0,
	ltob((uint16_t)sizeof(Elf32_Ehdr)),
	ltob((uint16_t)sizeof(Elf32_Phdr)) };

Assembler::Assembler(uint32_t pageAlign, MIPSArch arch, bool useGP) :
	pageAlign(pageAlign), arch(arch), bUseGP(useGP)
{
}

bool Assembler::loadAsm(std::istream &in)
{
	reset();

	std::string sLine;
	int lineID = 0;
	while (std::getline(in, sLine))
	{
		lineID++;
		sLine += ' ';
		vLines.push_back(parseLine(sLine, lineID));
		if (issError.size() > MaxError)
		{
			issError.push_back({ 0, "Too many errors, stop assembling." });
			return false;
		}
	}
	if (!issError.empty())
		return false;

	locateLabel();

	if (!issError.empty())
		return false;
	bLoaded = true;
	return true;
}

void Assembler::reset()
{
	issError.clear();
	issWarn.clear();
	bLoaded = false;
	vLines.clear();
	vLineAddr.clear();
	vLineOffset.clear();
	vLineInsCnt.clear();
	mapLabelLine.clear();
	mapLocalLLine.clear();
	sizeImage = 0;
	addrGP = 0;
}

std::vector<Assembler::Token> Assembler::parseLine(const std::string &line, int lineID)
{
	enum { Normal, HexNumber, String, StringEscape, StringEscapeOct, StringEscapeHex } status = Normal;
	size_t i = 0;
	std::vector<Token> ret;
	std::string last;
	last.reserve(line.size());
	int lastNum = 0, numCnt = 0;

	for (; i < line.size(); i++)
		if (!isSpace(line[i]))
			break;
	for (; i < line.size(); i++)
	{
		if (status == String)
		{
			if (line[i] == '\\')
				status = StringEscape;
			else if (line[i] == '\"')
				ret.push_back(Token(string, 0, last)), last.clear(), status = Normal;
			else
				last += line[i];
			continue;
		}
		else if (status == StringEscape)
		{
			if (line[i] == 'n')
				last += '\n';
			else if (line[i] == 't')
				last += '\t';
			else if (line[i] == '"')
				last += '\"';
			else if (line[i] == '\'')
				last += '\'';
			else if (line[i] == 'x')
			{
				status = StringEscapeHex, numCnt = 0;
				lastNum = 0;
				continue;
			}
			else if (line[i] >= '0' && line[i] <= '7')
			{
				status = StringEscapeOct, numCnt = 1;
				lastNum = line[i] - '0';
				continue;
			}
			else
				issWarn.push_back({ lineID, std::string("Invalid Escape Character '\\") + line[i] + "'" });
			status = String;
			continue;
		}
		else if (status == StringEscapeHex)
		{
			if (isHex(line[i]))
				lastNum *= 16, lastNum += hex2int(line[i]), numCnt++;
			else
			{
				if (numCnt == 0)
					issWarn.push_back({ lineID, std::string("Invalid Escape Character '\\x") + line[i] + "'" });
				else
					last += char(lastNum);
				status = String;
				continue;
			}

			if (numCnt >= 2)
				status = String, last += char(lastNum);
			continue;
		}
		else if (status == StringEscapeOct)
		{
			if (line[i] >= '0' && line[i] <= '7')
				lastNum *= 8, lastNum += line[i] - '0', numCnt++;
			else
			{
				status = String, last += char(lastNum);
				continue;
			}
			if (numCnt >= 3)
				status = String, last += char(lastNum);
			continue;
		}
		else if (status == HexNumber)
		{
			if (isHex(line[i]))
			{
				lastNum *= 16, lastNum += hex2int(line[i]), numCnt++;
				continue;
			}
			else
			{
				if (numCnt == 0)
				{
					issError.push_back({ lineID, "Invalid Hex Number '0x'" });
					return ret;
				}
				if (isBreak(line[i]))
				{
					status = Normal, ret.push_back(Token(number, lastNum));
					if (numCnt > 8)
						issWarn.push_back({ lineID, "Number '0x" + line.substr(i - numCnt, numCnt) + "' is too large." });
					last.clear();
				}
				else
				{
					issError.push_back({ lineID, "Invalid Hex Number '0x" + line.substr(i - numCnt, numCnt + 1) + "'" });
					return ret;
				}
			}
		}
		assert(status == Normal);
		
		if (line[i] == '#')
			break;

		if (isBreak(line[i]))
		{
			auto escape = [&]()
			{
				last.clear();
				if (line[i] == '(')
					ret.push_back(bracket_in);
				else if (line[i] == ')')
					ret.push_back(bracket_out);
			};
			if (last.empty())
			{
				escape();
				continue;
			}

			if (last[0] == '$')		//Register
			{
				auto iterReg = mapR.find(last.substr(1));
				if (iterReg != mapR.end())
				{
					ret.push_back(Token(reg, iterReg->second));
					escape();
					continue;
				}
				else if (checkNum(last.substr(1)))
				{
					int regid = atoi(last.substr(1).c_str());
					if (regid < RegCount && regid >= 0)
						ret.push_back(Token(reg, regid));
					escape();
					continue;
				}
			}
			if (last[0] == '%')		//Oper: %lo %hi
			{
				auto iterOper = mapO.find(last.substr(1));
				if (iterOper != mapO.end())
				{
					ret.push_back(Token(oper, iterOper->second));
					escape();
					continue;
				}
				else
				{
					issError.push_back({ lineID, "Unsupported Operation '" + last + "'" });
					return ret;
				}
			}
			if (last[0] == '.')
			{
				auto iterAD = mapASMerD.find(last.substr(1));
				if (iterAD != mapASMerD.end())
					ret.push_back(Token(asmd, iterAD->second));
				else
				{
					size_t dot = last.find('.', 1);			//.data.rel.local -> data
					if (dot != std::string::npos)
						dot--;
					auto iterS = mapS.find(last.substr(1, dot));
					if (iterS != mapS.end())
					{
						ret.push_back(Token(sect, iterS->second));
					}
					else
					{
						ret.push_back(Token(asmd, INVALID_ASMDirective));
						issWarn.push_back({ lineID, "Unknown Assembler Directive '" + last + "'" });
						return ret;
					}
				}
				escape();
				continue;
			}
			if (last[0] == '@')
			{
				ret.push_back(Token(unknown));
				issWarn.push_back({ lineID, "Unsupported syntax '" + last + "'" });
				escape();
				continue;
			}
			auto iterD = mapD.find(last);
			if (iterD != mapD.end())
			{
				ret.push_back(Token(directive, iterD->second));
				escape();
				continue;
			}
			if (last.back() == 'f' || last.back() == 'b')	//Local Label: 1f 2b
			{
				if (checkLabel(last.substr(0, last.size() - 1)) == 2)
				{
					ret.push_back(Token(labelname, atoi(last.substr(0, last.size() - 1).c_str()), last.substr(last.size() - 1)));
					escape();
					continue;
				}
			}
			if (checkNum(last))		//Dec Number
			{
				int num = atoi(last.c_str());
				std::stringstream ss;
				ss << num;
				if (ss.str() != last)
					issWarn.push_back({ lineID, "Number '" + last + "' is too large." });
				ret.push_back(Token(number, num));
				escape();
				continue;
			}
			if (checkLabel(last) == 1)	//Label
			{
				ret.push_back(Token(labelname, 0, last));
				escape();
				continue;
			}
			issError.push_back({ lineID, "Syntax Error: '" + last + "'" });
			return ret;
		}

		if (line[i] == ':')
		{
			int tmp = checkLabel(last);
			if (tmp == 1)
				ret.push_back(Token(label, 0, last));
			else if (tmp == 2)
				ret.push_back(Token(label, atoi(last.c_str())));
			else
			{
				issError.push_back({ lineID, "Invalid Label Name: '" + last + "'" });
				return ret;
			}
			status = Normal;
			last.clear();
		}
		else if (line[i] == 'x' && last == "0")
			status = HexNumber, lastNum = 0, numCnt = 0;
		else if (line[i] == '\"' && status != String)
			status = String;
		else
			last += line[i];
	}
	return ret;
}

void Assembler::locateLabel()
{
	const uint32_t addrProgram = baseAddr + sizeof(Elf32_Ehdr) + 2 * sizeof(Elf32_Phdr);
	const uint32_t offsetProgram = addrProgram - baseAddr;

	uint32_t curAddr = addrProgram, curOffset = addrProgram - baseAddr;
	uint32_t addrData = 0, offsetData = 0;

	Section order[] = { text, rdata, data, bss };
	Section curtype = INVALID_Section;
	vLineAddr.resize(vLines.size());
	vLineOffset.resize(vLines.size());
	vLineInsCnt.resize(vLines.size());
	for (auto &i : vLineAddr)
		i = 0;
	addrGP = 0;

	for (Section target_type : order)
	{
		if ((target_type == data || target_type == bss) && !addrData)
		{
			//curAddr = alignAddr(curAddr, pageAlign);
			if (curAddr & ((1 << pageAlign) - 1))
				curAddr += 1 << pageAlign;
			addrData = curAddr;
			offsetData = curOffset;
		}

		for (int lineID = 1; lineID <= vLines.size(); lineID++)
		{
			std::vector<Token> &line = vLines[lineID - 1];
			
			if (curtype == target_type)
			{
				vLineAddr[lineID - 1] = curAddr;
				vLineOffset[lineID - 1] = curOffset;
			}
			if (line.empty())
				continue;

			uint32_t oldAddr = curAddr;

			if (line[0].type == label)
			{
				if (curtype == INVALID_Section)
				{
					issError.push_back({ lineID, "Unspecified section type" });
					return;
				}
				if (curtype == target_type)
				{
					if (!newLabel(line[0], lineID))
					{
						issError.push_back({ lineID, "Deplicated label '" + line[0].name + "'" });
						return;
					}
					if (line.size() >= 2 && line[1].type == sect && line[1].value != curtype ||
						line.size() >= 3 && line[1].type == asmd && line[1].value == section && line[2].type == sect && line[2].value != curtype)
					{
						issWarn.push_back({ lineID, "Label on a section identifier" });
					}
					line.erase(line.begin());
				}
			}
			else if (line[0].type == sect || line[0].type == asmd && line[0].value == section)
			{
				if (line[0].type == sect)
					curtype = Section(line[0].value);
				else if (line.size() >= 2 && line[1].type == sect)
					curtype = Section(line[1].value);
				else
				{
					issError.push_back({ lineID, "Section Name Not Found" });
					return;
				}
			}
			else if (line[0].type == asmd && curtype == target_type)
			{
				if (!checkASMD(line))
				{
					issError.push_back({ lineID, "Invalid Argument" });
					return;
				}
				if (curtype == bss && line[0].value != space)
					issWarn.push_back({ lineID, "Data in bss section may be ignored" });
				switch (line[0].value)
				{
				case align:
					if (line[1].value > 30)
						issWarn.push_back({ lineID, "Incorrect Alignment" });
					curAddr = alignAddr(curAddr, line[1].value);
					break;
				case byte:
					curAddr += line.size() - 1;
					break;
				case space:
					if (line[1].value < 0 || curAddr + line[1].value > 0x7fffffff)
					{
						issError.push_back({ lineID, "Incorrect Space Size" });
						return;
					}
					curAddr += line[1].value;
					break;
				case word:
					curAddr += 4 * (line.size() - 1);
					break;
				case ascii:
					curAddr += line[1].name.size();
					break;
				case asciiz:
					curAddr += line[1].name.size() + 1;
				}
			}
			else if (line[0].type == directive && curtype == target_type)
			{
				if (curtype == bss)
					issWarn.push_back({ lineID, "Data in bss section may be ignored" });
				int nd = encodeInstruction(line, lineID, nullptr);
				assert(!issError.empty() || nd >= 1);
				if (arch == fake && nd > 1)
					issWarn.push_back({ lineID, "Marco Instruction Expanded" });
				vLineInsCnt[lineID - 1] = nd;
				if (issError.size() > MaxError)
					return;
				curAddr += 4 * nd;
			}
			else if(curtype == target_type)
				issWarn.push_back({ lineID, "Unknown syntax" });

			if(target_type != bss)
				curOffset += curAddr - oldAddr;
		}
	}
	sizeImage = curOffset;

	auto iterMain = mapLabelLine.find("main");
	if (iterMain == mapLabelLine.end())
	{
		issError.push_back({ 0, "Entry 'main' not found" });
		return;
	}

	memcpy(&elfHeader, &elfTemplateHeader, sizeof(Elf32_Ehdr));
	elfHeader.e_phoff = ltob((uint32_t)sizeof(Elf32_Ehdr));
	elfHeader.e_phnum = ltob((uint16_t)2);
	elfHeader.e_entry = ltob(vLineAddr[iterMain->second]);
	
	Elf32_Phdr phdr;

	if (offsetData == offsetProgram)
	{
		issWarn.push_back({ 0, "'.text' section not found" });
		memset(&phdr, 0, sizeof(phdr));
	}
	else
	{
		memset(&phdr, 0, sizeof(phdr));
		phdr.p_type = ltob(PT_LOAD);
		phdr.p_offset = 0;
		phdr.p_vaddr = ltob(baseAddr);
		phdr.p_paddr = ltob(baseAddr);
		phdr.p_filesz = ltob(offsetData);
		phdr.p_memsz = phdr.p_filesz;
		phdr.p_flags = ltob(PF_R | PF_X);
		phdr.p_align = ltob(1U << pageAlign);
	}
	vElfPhdr.push_back(phdr);
	if (curOffset == offsetData)
	{
		memset(&phdr, 0, sizeof(phdr));
	}
	else
	{
		memset(&phdr, 0, sizeof(phdr));
		phdr.p_type = ltob(PT_LOAD);
		phdr.p_offset = ltob(offsetData);
		phdr.p_vaddr = ltob(addrData);
		phdr.p_paddr = ltob(addrData);
		phdr.p_filesz = ltob(curOffset - offsetData);
		phdr.p_memsz = ltob(curAddr - addrData);
		phdr.p_flags = ltob(PF_R | PF_W);
		phdr.p_align = ltob(1U << pageAlign);
	}
	vElfPhdr.push_back(phdr);

	if (bUseGP)
		addrGP = addrData;
}

bool Assembler::generateImage(byte_t *buffer)
{
	if (!sizeImage || !bLoaded)
		return false;
	memset(buffer, 0, sizeImage);
	memcpy(buffer, &elfHeader, sizeof(Elf32_Ehdr));
	for (int i = 0; i < vElfPhdr.size(); i++)
		memcpy(buffer + sizeof(Elf32_Ehdr) + i*sizeof(Elf32_Phdr), &vElfPhdr[i], sizeof(Elf32_Phdr));

	for (int lineID = 1; lineID <= vLines.size(); lineID++)
	{
		std::vector<Token> &line = vLines[lineID - 1];
		if (line.empty())
			continue;
		uint32_t curOffset = vLineOffset[lineID - 1];
		if (curOffset == sizeImage)
			continue;

		if (line[0].type == asmd)
		{
			switch (line[0].value)
			{
			case byte:
				for (int i = 0; i < line.size() - 1; i++)
					*(buffer + curOffset + i) = (byte_t)line[i+1].value;
				break;
			case word:
				for (int i = 0; i < line.size() - 1; i++)
					*((uint32_t *)(buffer + curOffset + i * 4)) = ltob((uint32_t)line[i + 1].value);
				break;
			case ascii:
				memcpy(buffer + curOffset, line[1].name.data(), line[1].name.size());
				break;
			case asciiz:
				memcpy(buffer + curOffset, line[1].name.c_str(), line[1].name.size() + 1);
				break;
			}
		}
		else if (line[0].type == directive)
		{
			if (!encodeInstruction(line, lineID, buffer + curOffset))
				return false;
		}
	}
	return true;
}

int Assembler::encodeInstruction(const std::vector<Token> &line, int lineID, byte_t *buffer)
{
	static uint32_t temp[10];
	uint32_t *pdir, *phead;
	if (!buffer)
		pdir = phead = temp;
	else
		pdir = phead = (uint32_t *)buffer;

	struct argument_error
	{
		std::string description;
		argument_error() {}
		argument_error(const std::string &description) : description(description) {}
	};
	auto assume = [&](std::initializer_list<TokenType> tokens)
	{
		if (line.size() < tokens.size() + 1)
			throw argument_error("Missing Argument");
		size_t i = 1;
		for (TokenType type : tokens)
		{
			if (line[i++].type != type)
				throw argument_error("Incorrect Argument");
		}
	};
	auto check_imm = [](int im, const std::string &err = "", bool no_throw = false)
	{
		if ((int16_t)im != im)
		{
			if(!no_throw)
				throw argument_error(err.empty() ? "Immediate number out of range" : err);
			return false;
		}
		return true;
	};
	auto encodeLI = [&](int im, uint8_t reg)
	{
		uint16_t hi = (im & 0xffff0000) >> 16, lo = im & 0xffff;
		if (lo & 0x8000)
			hi++;
		if (hi)
		{
			*(pdir++) = encodeIType(Opcd::LUI, zero, reg, hi);
			if (lo)
			{
				*(pdir++) = encodeIType(Opcd::ADDIU, reg, reg, lo);
				return 2;
			}
			return 1;
		}
		else
		{
			*(pdir++) = encodeIType(Opcd::ADDIU, zero, reg, lo);
			return 1;
		}
	};
	auto plainRIns = [&](bool flag = false)
	{
		static const std::unordered_map<int, std::pair<OpFunc, Opcd>> mapFunc = {
			{ add, {OpFunc::ADD, Opcd::ADDI} },{ addu, {OpFunc::ADDU, Opcd::ADDIU} },
			{ and, {OpFunc::AND, Opcd::ANDI} },{ nor, {OpFunc::NOR, Opcd::SPECIAL} },
			{ or , {OpFunc::OR, Opcd::ORI} },{ slt, {OpFunc::SLT, Opcd::SLTI} },
			{ sltu, {OpFunc::SLTU, Opcd::SLTIU} },{ sub, {OpFunc::SUB, Opcd::SPECIAL} },
			{ subu, {OpFunc::SUBU, Opcd::SPECIAL} },{ xor, {OpFunc::XOR, Opcd::XORI} } };
		if (!flag)
		{
			assume({ reg, reg, reg });
			*(pdir++) = encodeRType(
				mapFunc.find((Directive)line[0].value)->second.first,
				line[2].value, line[3].value, line[1].value);
		}
		else
		{
			assume({ reg, reg, number });
			Opcd opcd = mapFunc.find((Directive)line[0].value)->second.second;
			assert(opcd != Opcd::SPECIAL);
			if (!check_imm(line[3].value, "", true))
			{
				encodeLI(line[3].value, at);
				*(pdir++) = encodeRType(
					mapFunc.find((Directive)line[0].value)->second.first,
					line[2].value, at, line[1].value);
			}
			else
				*(pdir++) = encodeIType(opcd, line[2].value, line[1].value, line[3].value);
		}
	};
	auto plainIIns = [&]()
	{
		static const std::unordered_map<int, Opcd> mapOpcd = {
			{ addi, Opcd::ADDI },{ addiu, Opcd::ADDIU },
			{ andi, Opcd::ANDI },{ ori, Opcd::ORI },
			{ slti, Opcd::SLTI },{ sltiu, Opcd::SLTIU },
			{ xori, Opcd::XORI } };
		assume({ reg, reg, number });
		if ((int16_t)line[3].value == line[3].value)
		{
			*(pdir++) = encodeIType(
				mapOpcd.find((Directive)line[0].value)->second,
				line[2].value, line[1].value, line[3].value);
		}
		else
		{
			issError.push_back({ lineID, "Immediate number out of range" });
			throw argument_error();
		}
	};
	
	auto divlikeIns = [&]()	//div-like instructions: div, divu, rem, remu
	{
		const bool sign = line[0].value == div || line[0].value == rem;
		const bool lo = line[0].value == div || line[0].value == divu;
		if (line.size() == 3)
		{
			assume({ reg, reg });
			if (arch == MIPS32r6)
				throw argument_error("'div' instruction with 2 params was removed in MIPSr6");
			else
				*(pdir++) = encodeRType(sign ? OpFunc::DIV : OpFunc::DIVU, line[1].value, line[2].value, zero);
		}
		else
		{
			if (line.size() >= 4 && line[3].type == number)
			{
				assume({ reg, reg, number });
				check_imm(line[3].value);
				if (arch == fake)
				{
					static const std::unordered_map<int, Opcd> mapOpcd = {
						{div, Opcd::DIVI_Fake}, {divu, Opcd::DIVIU_Fake},
						{rem, Opcd::REMI_Fake}, {remu, Opcd::REMIU_Fake} };
					*(pdir++) = encodeIType(mapOpcd.find((Directive)line[0].value)->second, line[2].value, line[1].value, line[3].value);
				}
				else if(arch == MIPS32r2)
				{
					//It's not good, I know.
					encodeLI(line[3].value, at);
					*(pdir++) = encodeRType(sign ? OpFunc::DIV : OpFunc::DIVU, line[2].value, at, zero);
					*(pdir++) = encodeRType(lo ? OpFunc::MFLO : OpFunc::MFHI, zero, zero, line[1].value);
				}
				else
				{
					encodeLI(line[3].value, at);
					*(pdir++) = encodeRType(sign ? OpFunc::SOP32 : OpFunc::SOP33, line[2].value, at, line[1].value, lo ? 0b10 : 0b11);
				}
			}
			else
			{
				assume({ reg, reg, reg });
				if (arch == MIPS32r2)
				{
					*(pdir++) = encodeRType(sign ? OpFunc::DIV : OpFunc::DIVU, line[2].value, line[3].value, zero);
					*(pdir++) = encodeRType(lo ? OpFunc::MFLO : OpFunc::MFHI, zero, zero, line[1].value);
				}
				else
					*(pdir++) = encodeRType(sign ? OpFunc::SOP32 : OpFunc::SOP33, line[2].value, line[3].value, line[1].value, lo ? 0b10 : 0b11);
			}
		}
	};
	auto slllikeIns = [&]()
	{
		static const std::unordered_map<int, OpFunc> mapFunc = {
			{sll, OpFunc::SLL}, {sllv, OpFunc::SLLV}, {sra, OpFunc::SRA}, {srav, OpFunc::SRAV},
			{srl, OpFunc::SRL}, {srlv, OpFunc::SRLV} };
		OpFunc func;
		if ((line[0].value == sllv || line[0].value == srav || line[0].value == srlv) && (line.size() >= 4 && line[3].type == reg))
			func = mapFunc.find((Directive)(line[0].value + 1))->second;
		else
			func = mapFunc.find((Directive)line[0].value)->second;
		if (func == OpFunc::SLLV || func == OpFunc::SRAV || func == OpFunc::SRLV)
		{
			assume({ reg, reg, reg });
			*(pdir++) = encodeRType(func, line[3].value, line[2].value, line[1].value);
		}
		else
		{
			assume({ reg, reg, number });
			*(pdir++) = encodeRType(func, zero, line[2].value, line[1].value, line[3].value & 0x1F);
		}
	};
	enum compare { EQU, NEQ, GT, GE, LT, LE };
	auto set_cmp_reg = [&](uint8_t rd, uint8_t rs, compare cmp, uint8_t rt, bool us = false)
	{
		switch (cmp)
		{
		case EQU:
			if (arch == fake)
				*(pdir++) = encodeRType(OpFunc::SLTU, rs, rt, rd, 0x2);
			else
			{
				*(pdir++) = encodeRType(OpFunc::XOR, rs, rt, rd);
				*(pdir++) = encodeIType(Opcd::SLTIU, rd, rd, 1);
			}
			break;
		case NEQ:
			if (arch == fake)
				*(pdir++) = encodeRType(OpFunc::SLTU, rs, rt, rd, 0x3);
			else
			{
				*(pdir++) = encodeRType(OpFunc::XOR, rs, rt, rd);
				*(pdir++) = encodeRType(OpFunc::SLTU, zero, rd, rd);
			}
			break;
		case GT:
			*(pdir++) = encodeRType(us ? OpFunc::SLTU : OpFunc::SLT, rt, rs, rd);
			break;
		case GE:
			if (arch == fake)
				*(pdir++) = encodeRType(us ? OpFunc::SLTU : OpFunc::SLT, rs, rt, rd, 0x1);
			else
			{
				*(pdir++) = encodeRType(us ? OpFunc::SLTU : OpFunc::SLT, rs, rt, rd);
				*(pdir++) = encodeIType(Opcd::XORI, rd, rd, 1);
			}
			break;
		case LT:
			*(pdir++) = encodeRType(us ? OpFunc::SLTU : OpFunc::SLT, rs, rt, rd);
			break;
		case LE:
			if (arch == fake)
				*(pdir++) = encodeRType(us ? OpFunc::SLTU : OpFunc::SLT, rt, rs, rd, 0x1);
			else
			{
				*(pdir++) = encodeRType(us ? OpFunc::SLTU : OpFunc::SLT, rt, rs, rd);
				*(pdir++) = encodeIType(Opcd::XORI, rd, rd, 1);
			}
			break;
		}
	};
	auto set_cmp_imm = [&](uint8_t rt, uint8_t rs, compare cmp, int im, bool us = false)
	{
		//check_imm(im); check_imm(im + 1);
		if (!check_imm(im, "", true) || !check_imm(im, "", true))
		{
			encodeLI(im, at);
			set_cmp_reg(rt, rs, cmp, at, us);
			return;
		}
		switch (cmp)
		{
		case EQU:
			if (arch == fake)
				*(pdir++) = encodeIType(Opcd::SEQI_Fake, rs, rt, im);
			else
			{
				*(pdir++) = encodeIType(Opcd::XORI, rs, rt, im);	//xori %rt, %rs, im
				*(pdir++) = encodeIType(Opcd::SLTIU, rt, rt, 1);	//sltiu	%rt, %rt, 1
			}
			break;
		case NEQ:
			if (arch == fake)
				*(pdir++) = encodeIType(Opcd::SNEI_Fake, rs, rt, im);
			else
			{
				*(pdir++) = encodeIType(Opcd::XORI, rs, rt, im);
				*(pdir++) = encodeRType(OpFunc::SLTU, zero, rt, rt);
			}
			break;
		case GT:
			if (arch == fake)
				*(pdir++) = encodeIType(us ? Opcd::SGEIU_Fake : Opcd::SGEI_Fake, rs, rt, im + 1);
			else
			{
				encodeLI(im, at);
				*(pdir++) = encodeRType(us ? OpFunc::SLTU : OpFunc::SLT, at, rs, rt);
			}
			break;
		case GE:
			if (arch == fake)
				*(pdir++) = encodeIType(us ? Opcd::SGEIU_Fake : Opcd::SGEI_Fake, rs, rt, im);
			else
			{
				encodeLI(im, at);
				*(pdir++) = encodeRType(us ? OpFunc::SLTU : OpFunc::SLT, rs, at, rt);	//slt %rt, %rs, %at
				*(pdir++) = encodeIType(Opcd::XORI, rt, rt, 1);		//xori %rt, %rt, 1
			}
			break;
		case LT:
			*(pdir++) = encodeIType(us ? Opcd::SLTIU : Opcd::SLTI, rs, rt, im);
			break;
		case LE:
			*(pdir++) = encodeIType(us ? Opcd::SLTIU : Opcd::SLTI, rs, rt, im + 1);
			break;
		}
	};
	
	auto get_address = [&](int lineID) -> uint32_t
	{
		if (!buffer)
			return 0;
		if (!lineID)
			throw argument_error("Label not found");
		return vLineAddr[lineID - 1];
	};
	
	uint32_t addrCur = buffer ? vLineAddr[lineID - 1] + 4 * vLineInsCnt[lineID-1]: 0;	//Note: offset = addrJump - (addrNow + 4)
	try
	{
		switch (line[0].value)
		{
		case abs:
			assume({ reg, reg });	//abs %dest, %src
			*(pdir++) = encodeIType(Opcd::BGEZ, line[2].value, zero, 2);	//bgez %src, [2]
			*(pdir++) = encodeRType(OpFunc::OR, line[2].value, zero, line[1].value);	//move %dest, %src
			*(pdir++) = encodeRType(OpFunc::SUB, zero, line[2].value, line[1].value);	//neg %dest, %src
			break;
		case add: case addu: case and: case nor: case or : case sub: case subu: case xor:
			if (line.size() >= 4 && line[3].type == number)
			{
				assume({ reg, reg, number });
				if (line[0].value == nor)
				{
					*(pdir++) = encodeIType(Opcd::ORI, line[2].value, line[1].value, line[3].value);	//ori %dest, %src, im
					*(pdir++) = encodeRType(OpFunc::NOR, line[1].value, zero, line[1].value);	//nor %dest, %dest, 0
				}
				else if (line[0].value == sub || line[0].value == subu)
				{
					Opcd opcd = line[0].value == sub ? Opcd::ADDI : Opcd::ADDIU;
					int im = -line[3].value;
					if (!check_imm(im, "", true))
					{
						encodeLI(im, at);
						*(pdir++) = encodeRType(line[0].value == sub ? OpFunc::ADD : OpFunc::ADDU, line[2].value, at, line[1].value);
					}
					else
						*(pdir++) = encodeIType(opcd, line[2].value, line[1].value, im);
				}
				else
					plainRIns(true);
			}
			else
				plainRIns();
			break;
		case addi: case addiu: case andi: case ori: case xori:
			plainIIns();
			break;
		case div: case divu: case rem: case remu:
			divlikeIns();
			break;
		case mul:
			if (line.size() >= 4 && line[3].type == number)
			{
				assume({ reg, reg, number });
				check_imm(line[3].value);
				if (arch == fake)
					*(pdir++) = encodeIType(Opcd::MULI_Fake, line[2].value, line[1].value, line[3].value);
				else if (arch == MIPS32r2)
				{
					encodeLI(line[3].value, at);
					*(pdir++) = encodeRType((OpFunc)0b10, line[2].value, at, line[1].value, 0, Opcd::SPECIAL2);
				}
				else
				{
					encodeLI(line[3].value, at);
					*(pdir++) = encodeRType(OpFunc::SOP30, line[2].value, at, line[1].value, 0b10);
				}
			}
			else
			{
				assume({ reg, reg, reg });
				if(arch == MIPS32r2)
					*(pdir++) = encodeRType((OpFunc)0b10, line[2].value, line[3].value, line[1].value, 0, Opcd::SPECIAL2);
				else
					*(pdir++) = encodeRType(OpFunc::SOP30, line[2].value, line[3].value, line[1].value, 0b10);
			}
			break;
		//Skip mulo, mulou
		case mult:
			assume({ reg, reg });
			if (arch == MIPS32r6)
				throw argument_error("'mult' with 2 params was removed in MIPS32r6");
			*(pdir++) = encodeRType(OpFunc::MULT, line[1].value, line[2].value, zero);
			break;
		case multu:
			assume({ reg, reg });
			if (arch == MIPS32r6)
				throw argument_error("'multu' with 2 params was removed in MIPS32r6");
			*(pdir++) = encodeRType(OpFunc::MULTU, line[1].value, line[2].value, zero);
			break;
		case neg:
			assume({ reg, reg });
			*(pdir++) = encodeRType(OpFunc::SUB, zero, line[2].value, line[1].value);
			break;
		case negu:
			assume({ reg, reg });
			*(pdir++) = encodeRType(OpFunc::SUBU, zero, line[2].value, line[1].value);
			break;
		case not:
			assume({ reg, reg });
			*(pdir++) = encodeRType(OpFunc::NOR, zero, line[2].value, line[1].value);
			break;
		case rol:
			if (line.size() >= 4 && line[3].type == number)
			{
				assume({ reg, reg, number });
				*(pdir++) = encodeRType(OpFunc::SRL, 0x1, line[2].value, line[1].value, 32 - (line[3].value & 0x1F));	//ROTR
			}
			else
			{
				assume({ reg, reg, reg });
				*(pdir++) = encodeRType(OpFunc::SUBU, zero, line[3].value, line[3].value);
				*(pdir++) = encodeRType(OpFunc::SRLV, line[3].value, line[2].value, line[1].value, 0x1);
			}
			break;
		case ror:
			if (line.size() >= 4 && line[3].type == number)
			{
				assume({ reg, reg, number });
				*(pdir++) = encodeRType(OpFunc::SRL, 0x1, line[2].value, line[1].value, line[3].value & 0x1F);
			}
			else
			{
				assume({ reg, reg, reg });
				*(pdir++) = encodeRType(OpFunc::SRLV, line[3].value, line[2].value, line[1].value, 0x1);
			}
			break;
		case sll: case sllv: case sra: case srav: case srl: case srlv:
			slllikeIns();
			break;
		case li:
			assume({ reg, number });
			encodeLI(line[2].value, line[1].value);
			break;
		case lui:
			assume({ reg, number });
			*(pdir++) = encodeIType(Opcd::LUI, zero, line[1].value, line[2].value);
			break;
		case seq: case sge: case sgt: case sle: case sne: case slt:
		{
			static const std::unordered_map<int, compare> mapCmp = {
				{ seq, EQU },{ sge, GE },{ sgt, GT },{ sle, LE },{ sne, NEQ },{slt, LT} };
			if (line.size() >= 4 && line[3].type == number)
			{
				assume({ reg, reg, number });
				set_cmp_imm(line[1].value, line[2].value, mapCmp.find((Directive)line[0].value)->second, line[3].value);
			}
			else
			{
				assume({ reg, reg, reg });
				set_cmp_reg(line[1].value, line[2].value, mapCmp.find((Directive)line[0].value)->second, line[3].value);
			}
			break;
		}
		case sgeu: case sgtu: case sleu: case sltu:
		{
			static const std::unordered_map<int, compare> mapCmp = {
				{sgeu, GE}, {sgtu, GT}, {sleu, LE}, {sltu, LT} };
			if (line.size() >= 4 && line[3].type == number)
			{
				assume({ reg, reg, number });
				set_cmp_imm(line[1].value, line[2].value, mapCmp.find((Directive)line[0].value)->second, line[3].value, true);
			}
			else
			{
				assume({ reg, reg, reg });
				set_cmp_reg(line[1].value, line[2].value, mapCmp.find((Directive)line[0].value)->second, line[3].value, true);
			}
			break;
		}
		case j: case b: case jal:
		{
			if (line.size() >= 2 && line[1].type == reg)
			{
				assume({ reg });
				if (arch == MIPS32r2)
					*(pdir++) = encodeRType(OpFunc::JR, line[1].value, zero, zero);
				else
					*(pdir++) = encodeRType(OpFunc::JALR, line[1].value, zero, zero);
			}
			else
			{
				assume({ labelname });
				uint32_t addr = get_address(findLabel(line[1], lineID));
				//uint32_t addrCur = vLineAddr[lineID - 1] + 4;
				if (arch == MIPS32r2)
				{
					if ((addr & 0xf0000000) != (addrCur & 0xf0000000))
					{
						throw argument_error("Address out of range");
						/*encodeLI(addr, at);
						if (line[0].value == jal)
							*(pdir++) = encodeRType(OpFunc::JALR, at, zero, ra);
						else
							*(pdir++) = encodeRType(OpFunc::JR, at, zero, zero);*/
					}
					else
					{
						if (line[0].value == jal)
							*(pdir++) = encodeJType(Opcd::JAL, (addr >> 2) & 0x03ffffff);
						else
							*(pdir++) = encodeJType(Opcd::J, (addr >> 2) & 0x03ffffff);
					}
				}
				else
				{
					int offset = addr - addrCur;
					if (std::abs(offset) & 0xf0000000)
					{
						throw argument_error("Address out of range");
						/*encodeLI(addr, at);
						if (line[0].value == jal)
							*(pdir++) = encodeRType(OpFunc::JALR, at, zero, ra);
						else
							*(pdir++) = encodeRType(OpFunc::JALR, at, zero, zero);*/
					}
					else
					{
						offset /= 4;
						if (line[0].value == jal)
							*(pdir++) = encodeJType(Opcd::BALC, offset & 0x03ffffff);
						else
							*(pdir++) = encodeJType(Opcd::BC, offset & 0x03ffffff);
					}
				}
			}
			break;
		}
		case jr:
			assume({ reg });
			if (arch == MIPS32r2)
				*(pdir++) = encodeRType(OpFunc::JR, line[1].value, zero, zero);
			else
				*(pdir++) = encodeRType(OpFunc::JALR, line[1].value, zero, zero);
			break;
		case beq: case bne: case bgt: case bge: case blt: case ble:
		{
			static const std::unordered_map<int, compare> mapCmp = {
				{ beq, EQU },{ bne, NEQ },{ bgt, GT },{ bge, GE },{ blt, LT },{ ble, LE } };

			if (line.size() < 4 || line[3].type != labelname)
				throw argument_error();
			uint32_t addr = get_address(findLabel(line[3], lineID));
			uint16_t offset = int(addr - addrCur) / 4;

			if (line.size() >= 3 && line[2].type == number)
			{
				assume({ reg, number, labelname });
				
				if (arch == fake)
				{
					static const std::unordered_map<int, Opcd> mapOpcd = {
						{beq, Opcd::BEQI_FakeUgly}, {bne, Opcd::BNEI_FakeUgly},
						{bgt, Opcd::BGEI_FakeUgly}, {bge, Opcd::BGEI_FakeUgly},
						{blt, Opcd::BLTI_FakeUgly}, {ble, Opcd::BLTI_FakeUgly} };
					int im = line[2].value;
					if (im <= -512 || im >= 511)
					{
						//throw argument_error("Imm number out of range");
						set_cmp_imm(at, line[1].value, mapCmp.find((Directive)line[0].value)->second, line[2].value);
						*(pdir++) = encodeIType(Opcd::BNE, at, zero, offset);
					}
					else
					{
						if (line[0].value == bgt || line[0].value == ble)
							im++;
						*(pdir++) = encodeUType(mapOpcd.find((Directive)line[0].value)->second, im & 0x3ff, line[1].value, offset & 0x7ff);
					}
				}
				else
				{
					set_cmp_imm(at, line[1].value, mapCmp.find((Directive)line[0].value)->second, line[2].value);
					*(pdir++) = encodeIType(Opcd::BNE, at, zero, offset);
				}
			}
			else
			{
				assume({ reg, reg, labelname });
				if (line[0].value == beq)
					*(pdir++) = encodeIType(Opcd::BEQ, line[1].value, line[2].value, offset);
				else if (line[0].value == bne)
					*(pdir++) = encodeIType(Opcd::BNE, line[1].value, line[2].value, offset);
				else if(arch == fake)
				{
					static const std::unordered_map<int, Opcd> mapOpcd = {
						{bgt, Opcd::BGT_Fake}, {bge, Opcd::BGE_Fake}, {blt, Opcd::BLT_Fake}, {ble, Opcd::BLE_Fake} };
					*(pdir++) = encodeIType(mapOpcd.find((Directive)line[0].value)->second, line[1].value, line[2].value, offset);
				}
				else
				{
					set_cmp_reg(at, line[1].value, mapCmp.find((Directive)line[0].value)->second, line[2].value);
					*(pdir++) = encodeIType(Opcd::BNE, at, zero, offset);
				}
			}
			break;
		}
		case bgtu: case bgeu: case bltu: case bleu:
		{
			static const std::unordered_map<int, compare> mapCmp = {
				{ bgtu, GT },{ bgeu, GE },{ bltu, LT },{ bleu, LE } };

			if (line.size() < 4 || line[3].value != labelname)
				throw argument_error();
			uint32_t addr = get_address(findLabel(line[3], lineID));
			uint16_t offset = int(addr - addrCur) / 4;

			if (line.size() >= 3 && line[2].type == number)
			{
				assume({ reg, number, labelname });
				if (arch == fake)
					throw argument_error("I'm tired... DO NOT be ugly");
				else
				{
					set_cmp_imm(at, line[1].value, mapCmp.find((Directive)line[0].value)->second, line[2].value, true);
					*(pdir++) = encodeIType(Opcd::BNE, at, zero, offset);
				}
			}
			else
			{
				assume({ reg, reg, labelname });
				if (arch == fake)
					throw argument_error("I'm really tired...");
				else
				{
					set_cmp_reg(at, line[1].value, mapCmp.find((Directive)line[0].value)->second, line[2].value, true);
					*(pdir++) = encodeIType(Opcd::BNE, at, zero, offset);
				}
			}
			break;
		}
		case beqz: case bnez: case bgtz: case bgez: case bltz: case blez:
		case bgezal: case bltzal:
		{
			assume({ reg, labelname });
			uint32_t addr = get_address(findLabel(line[2], lineID));
			uint16_t offset = int(addr - addrCur) / 4;

			static const std::unordered_map<int, Opcd> mapOpcd = {
				{ beqz, Opcd::BEQ }, { bnez, Opcd::BNE },
				{ bgtz, Opcd::BGTZ },{ bgez, Opcd::BGTZ },{ bltz, Opcd::BLTZ },{ blez, Opcd::BLEZ } };

			if (line[0].value == bgez)
				*(pdir++) = encodeIType(Opcd::BGEZ, line[1].value, 1, offset);
			else if (line[0].value == bgezal)
			{
				if (arch == MIPS32r2)
					*(pdir++) = encodeIType(Opcd::BLTZ, line[1].value, 0b10001, offset);
				else
					*(pdir++) = encodeIType(Opcd::POP06, line[1].value, line[1].value, offset);
			}
			else if (line[0].value == bltzal)
			{
				if (arch == MIPS32r2)
					*(pdir++) = encodeIType(Opcd::BLTZ, line[1].value, 0b10000, offset);
				else
					*(pdir++) = encodeIType(Opcd::POP07, line[1].value, line[1].value, offset);
			}
			else
				*(pdir++) = encodeIType(mapOpcd.find((Directive)line[0].value)->second, line[1].value, zero, offset);
			break;
		}
		case la: case lb: case lbu: case ld: case lh: case lhu: case lw:
		case sb: case sd: case sh: case sw:
		{
			static const std::unordered_map<int, Opcd> mapOpcd = {
				{lb, Opcd::LB}, {lbu, Opcd::LBU}, {lh, Opcd::LH}, {lhu, Opcd::LHU}, {lw, Opcd::LW},
				{sb, Opcd::SB}, {sh, Opcd::SH}, {sw, Opcd::SW} };
			uint8_t regAddr, regTarget;
			uint16_t offset;
			if (line.size() >= 3 && line[2].type == number)
			{
				assume({ reg, number, bracket_in, reg, bracket_out });
				check_imm(line[2].value);
				regAddr = line[4].value;
				offset = line[2].value;
			}
			else if (bUseGP)
			{
				assume({ reg, labelname });
				uint32_t addr = get_address(findLabel(line[2], lineID));
				check_imm(addr - addrGP, "Address is too far from GP");
				offset = addr - addrGP;
				regAddr = gp;
			}
			else
			{
				assume({ reg, labelname });
				uint32_t addr = get_address(findLabel(line[2], lineID));
				if (encodeLI(addr, at) == 1)
					*(pdir++) = encodeRType((OpFunc)0, 0, 0, 0);
				offset = 0;
				regAddr = at;
			}
			regTarget = line[1].value;

			if (line[0].value == la)
				*(pdir++) = encodeIType(Opcd::ADDIU, regAddr, regTarget, offset);
			else if (line[0].value == ld)
			{
				//Is it correct?
				*(pdir++) = encodeIType(Opcd::LW, regAddr, regTarget, offset);
				*(pdir++) = encodeIType(Opcd::LW, regAddr, regTarget + 1, offset + 4);
			}
			else if (line[0].value == sd)
			{
				*(pdir++) = encodeIType(Opcd::SW, regAddr, regTarget, offset);
				*(pdir++) = encodeIType(Opcd::SW, regAddr, regTarget + 1, offset + 4);
			}
			else
			{
				*(pdir++) = encodeIType(mapOpcd.find((Directive)line[0].value)->second, regAddr, regTarget, offset);
			}
			break;
		}
		case move:
			assume({ reg, reg });
			*(pdir++) = encodeRType(OpFunc::OR, line[2].value, zero, line[1].value);
			break;
		case mfhi:
			assume({ reg });
			if (arch == MIPS32r6)
				throw argument_error("'MFHI' was removed in MIPSr6");
			*(pdir++) = encodeRType(OpFunc::MFHI, zero, zero, line[1].value);
			break;
		case mflo:
			assume({ reg });
			if (arch == MIPS32r6)
				throw argument_error("'MFLO' was removed in MIPSr6");
			*(pdir++) = encodeRType(OpFunc::MFLO, zero, zero, line[1].value);
			break;
		case nop:
			*(pdir++) = encodeIType((Opcd)0, 0, 0, 0);	//sll $zero, $zero, 0
			break;
		case syscall:
			*(pdir++) = encodeRType(OpFunc::SYSCALL, zero, zero, zero);
			break;
		default:
			throw argument_error("Internal Error");
		}
	}
	catch (argument_error &e)
	{
		if (e.description.empty())
			issError.push_back({ lineID, "Argument Error" });
		else
			issError.push_back({ lineID, e.description });
		return 0;
	}
	return pdir - phead;
}

int Assembler::checkLabel(const std::string &label)
{
	if (label.empty())
		return 0;
	if (label[0] >= '0' && label[0] <= '9')
	{
		for (size_t i = 1; i < label.size(); i++)
			if (label[i] < '0' || label[i] > '9')
				return 0;
		return 2;	//Local Symbol Name
	}
	for (size_t i = 0; i < label.size(); i++)
	{
		if (!(label[i] >= 'a' && label[i] <= 'z' || label[i] >= 'A' && label[i] <= 'Z' || label[i] >= '0' && label[i] <= '9' || label[i] == '_' || label[i] == '$'))
			return 0;
	}
	return 1;
}

bool Assembler::checkASMD(const std::vector<Token> &line)
{
	assert(line[0].type == asmd);
	switch (line[0].value)
	{
	case align: case space: 
		if (line.size() < 2 || line[1].type != number)
			return false;
		break;
	case byte: case word:
		if (line.size() < 2)
			return false;
		for (int i = 0; i < line.size() - 1; i++)
			if (line[i + 1].type != number)
				return false;
		break;
	case ascii: case asciiz:
		if (line.size() < 2 || line[1].type != string)
			return false;
	}
	return true;
}

bool Assembler::newLabel(const Token &label, int lineID)
{
	assert(label.type == TokenType::label);
	if (!label.value)
	{
		if (mapLabelLine.count(label.name) != 0)
			return false;
		mapLabelLine.insert({ label.name, lineID });
		return true;
	}
	else
	{
		auto iter = mapLocalLLine.find(label.value);
		if (iter == mapLocalLLine.end())
			mapLocalLLine.insert({ label.value, std::set<int>({lineID}) });
		else
			iter->second.insert(lineID);
		return true;
	}
}

int Assembler::findLabel(const Token &label, int lineID)
{
	assert(label.type == labelname);
	if (!label.value)
	{
		auto iter = mapLabelLine.find(label.name);
		if (iter == mapLabelLine.end())
			return 0;
		return iter->second;
	}
	else
	{
		auto iterSet = mapLocalLLine.find(label.value);
		if (iterSet == mapLocalLLine.end())
			return 0;
		if (label.name == "f")
		{
			auto iter = iterSet->second.upper_bound(lineID);
			if (iter == iterSet->second.end())
				return 0;
			return *iter;
		}
		else if (label.name == "b")
		{
			if (iterSet->second.empty())
				return 0;
			if (*iterSet->second.begin() >= lineID)
				return 0;
			auto iter = --iterSet->second.lower_bound(lineID);
			assert(iter != iterSet->second.end());
			return *iter;
		}
	}
	return 0;
}
