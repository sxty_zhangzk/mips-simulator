mips-simulator: 
	mkdir bin/
	g++ -c src/assembler.cpp -o bin/assembler.o -std=c++11 -fno-operator-names -O2
	g++ -c src/cpu_pipelined.cpp -o bin/cpu_pipelined.o -std=c++11 -fno-operator-names -O2
	g++ -c src/umode.cpp -o bin/umode.o -std=c++11 -fno-operator-names -O2
	g++ -c src/main.cpp -o bin/main.o -std=c++11 -fno-operator-names -O2
	g++ bin/assembler.o bin/cpu_pipelined.o bin/umode.o bin/main.o -o bin/mips-simulator 

clean: 
	rm -rf ./bin
all: mips-simulator
